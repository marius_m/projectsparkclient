package org.example;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import javafx.application.Application;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class App extends Application {
    @FXML
    private TableView<List<StringProperty>> Table;
    @FXML
    private TableColumn<List<StringProperty>, String> C1;
    @FXML
    private TableColumn<List<StringProperty>, String> C2;
    @FXML
    private TableColumn<List<StringProperty>, String> C3;
    @FXML
    private TableColumn<List<StringProperty>, String> C4;
    @FXML
    private TableColumn<List<StringProperty>, String> C5;
    @FXML
    private Button Button;
    @FXML
    private TextField BucketField;
    @FXML
    private Label ErrorPrompt;
    @FXML
    private Button ButtonHelp;

    @Override
    public void start(Stage stage) {
        Scene scene = null;

        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("../../SparkClientFxml.fxml")); //create loader for the GUI layout
            loader.setController(this);
            StackPane stackPane = loader.load(); //load GUI elements
            scene = new Scene(stackPane, 1280, 720); //create scene
        } catch (IOException e) {
            e.printStackTrace();
        }
        stage.setScene(scene);

        Button.setOnAction(event -> getAWSData()); //add click handler for loading data

        ButtonHelp.setOnAction(event -> showHelpDialog()); //add click handler for the tooltip

        stage.setResizable(false);
        stage.show();
    }

    private void showHelpDialog() { // Show helpdialog for aws credentials
        TextArea textArea = new TextArea("You have to create a .aws file with your credentials on your PC." +
                " https://docs.aws.amazon.com/sdk-for-php/v3/developer-guide/guide_credentials_profiles.html");
        textArea.setEditable(false);
        textArea.setWrapText(true);
        GridPane gridPane = new GridPane();
        gridPane.setMaxWidth(Double.MAX_VALUE);
        gridPane.add(textArea, 0, 0);

        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Setup AWS Credentials");
        alert.getDialogPane().setContent(gridPane);

        alert.showAndWait();
    }

    private void getAWSData() {
        ErrorPrompt.setVisible(false);

        if(BucketField.getText().isEmpty()) { //check if an bucket name is given
            ErrorPrompt.setText("You have to enter a bucket name.");
            ErrorPrompt.setVisible(true);
            return;
        }

        Regions clientRegion = Regions.US_EAST_1; //predefined by the related SparkApplication
        String bucketName = BucketField.getText();
        String key = "Top_Hashtags.txt"; //predefined by the related SparkApplication

        S3Object fullObject = null, objectPortion = null, headerOverrideObject = null;
        
        try {
            AmazonS3 s3Client = AmazonS3ClientBuilder.standard() //build a S3 client
                    .withRegion(clientRegion)
                    .build();

            fullObject = s3Client.getObject(new GetObjectRequest(bucketName, key)); //retrieve the file

            displayTextInputStream(fullObject.getObjectContent());
        }catch (AmazonServiceException e) {
            ErrorPrompt.setText("An error occured when connecting to Amazon S3.");
            ErrorPrompt.setVisible(true);
            e.printStackTrace();
        } 
        catch (java.io.IOException e) {
            ErrorPrompt.setText("An error occured when trying to read the data.");
            ErrorPrompt.setVisible(true);
            e.printStackTrace();
        } 
        finally {
            // To ensure that the network connection doesn't remain open, close any open input streams.
            if (fullObject != null) {
                try {
                    fullObject.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (objectPortion != null) {
                try {
                    objectPortion.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (headerOverrideObject != null) {
                try {
                    headerOverrideObject.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void displayTextInputStream(InputStream input) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(input, "UTF8"));
        String line = reader.readLine(); //read the content of the text file
        if (line != null) {
            convertTextToTable(line);
        }
    }

    private void convertTextToTable(String line){
        List<String[]> listOfArr = new ArrayList<>();

        StringBuffer text = new StringBuffer(line);
        text.delete(0,5); //Delete leading 'List('
        text.delete(text.length()-1,text.length()); //Delete last char ')'

        final String regex = "(\\S*)\\s"; // regular expression to extract the individual related values

        final Pattern pattern = Pattern.compile(regex, Pattern.MULTILINE);
        final Matcher matcher = pattern.matcher(text); //apply regex on the data String

        while (matcher.find()) { //go through every "row"
            for (int i = 1; i <= matcher.groupCount(); i++) { //go through every single entry of an "row"
                listOfArr.add(matcher.group(i).split(",")); //split every entry with ","
            }
        }

        //define to take the corresponding value of every column
        C1.setCellValueFactory(data -> data.getValue().get(0));
        C2.setCellValueFactory(data -> data.getValue().get(1));
        C3.setCellValueFactory(data -> data.getValue().get(2));
        C4.setCellValueFactory(data -> data.getValue().get(3));
        C5.setCellValueFactory(data -> data.getValue().get(4));

        ObservableList<List<StringProperty>> data = FXCollections.observableArrayList();

        for (String[] row : listOfArr) { //go through every row to display
            List<StringProperty> rowToAdd = new ArrayList<>();
            for (int i = 0; i <row.length;i++){
                rowToAdd.add(i,new SimpleStringProperty(row[i])); //add the data to the row
            }
            data.add(rowToAdd); //add row to ObservableList
        }

        Table.setItems(data); //display data in tableview
    }


    public static void main(String[] args) {
        launch();
    }

}
